-- phpMyAdmin SQL Dump
-- version 4.3.8
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 14, 2018 at 03:42 PM
-- Server version: 5.5.51-38.2
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `quinnedy_portfolio`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pitems`
--

CREATE TABLE IF NOT EXISTS `tbl_pitems` (
  `pitems_id` tinyint(2) unsigned NOT NULL,
  `pitems_image` varchar(50) NOT NULL,
  `pitems_title` varchar(60) NOT NULL,
  `pitems_desc` text NOT NULL,
  `pitems_link_src` varchar(50) NOT NULL,
  `pitems_link_text` varchar(30) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_pitems`
--

INSERT INTO `tbl_pitems` (`pitems_id`, `pitems_image`, `pitems_title`, `pitems_desc`, `pitems_link_src`, `pitems_link_text`) VALUES
(1, 'batflip.jpg', 'UWO Ivey Program Campaign Poster', 'This poster was commissioned by the UWO Ivey student Joe Laferiere for his campaign to be Vice President of Social activities for his section of the Ivey program.  The client request only that the poster be based off the Jose Bautista bat flip image, and gave complete creative control otherwise.  ', '', ''),
(2, 'the_moon.png', 'Personal Project:  The Moon', 'This was a personal project done for particular reason.  My goal with this was to combine the influences of animation styles such as Adventure Time and the aesthetic of vintage posters to see what I could make.', '', ''),
(3, 'videothumb.jpg', 'Team Canada 1972 Intro Video', 'In 2017 I had the opportunity to work on a team to help develop a website and branding for a foundation being formed by the original Team Canada.  As part of the project, I edited together a video that told the story of the 1972 hockey series between Canada and Russia.', 'https://youtu.be/SuDYsaPwoIQ', 'See The Full Video Here'),
(4, 'hairandwardrobe.png', 'MacLachlan''s Hair and Wardrobe', 'For this project the client required a full range of services, from branding to development of a new website.  MacLachlan Hair and Wardrobe is a fully responsive site, optimized for a variety of different viewing devices.  The site is currently in the final stages of development, so check back here soon to find a link.', '#', 'Check Back Later');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_skills`
--

CREATE TABLE IF NOT EXISTS `tbl_skills` (
  `skills_id` tinyint(2) unsigned NOT NULL,
  `skills_icon` varchar(30) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_skills`
--

INSERT INTO `tbl_skills` (`skills_id`, `skills_icon`) VALUES
(1, 'html_icon.png'),
(2, 'css_icon.png'),
(3, 'js_icon.png'),
(4, 'php_icon.png'),
(5, 'adobe_suite_icon.png'),
(6, 'git_icon.png'),
(7, 'grunt_icon.png'),
(8, 'sass_icon.png');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_pitems`
--
ALTER TABLE `tbl_pitems`
  ADD PRIMARY KEY (`pitems_id`);

--
-- Indexes for table `tbl_skills`
--
ALTER TABLE `tbl_skills`
  ADD PRIMARY KEY (`skills_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_pitems`
--
ALTER TABLE `tbl_pitems`
  MODIFY `pitems_id` tinyint(2) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tbl_skills`
--
ALTER TABLE `tbl_skills`
  MODIFY `skills_id` tinyint(2) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
