(function () {

  const right = document.querySelector('#right');
  const left = document.querySelector('#left');
  var counter = 1;



  function loadup(){
    const url = './includes/functions.php?id=' + counter;
    fetch(url)
      .then((resp) => resp.json())
      .then((data) => { processResultLoad(data); })
      .catch(function(error) {
        console.log(error);
      });
  }

  function loadSkills(){
   const url = './includes/functions.php?skills=true';
    fetch(url)
      .then((resp) => resp.json())
     .then((data) => {processResultSkills(data); })
      .catch(function(error) {
        console.log(error);
      });
  }

 function processResultSkills(data){
  let iconHolder = document.querySelector('#tList');
  for (var i = 0; i < data.length; i ++){
    let docFrag = '<li class="skillsList"><img class = "listImage" src="img/'+data[i].skills_icon+'"</li>';
    iconHolder.innerHTML += docFrag;
  }

  }

  function getNext(){
    counter = counter + 1;
    const url = './includes/functions.php?id=' + counter;

    fetch(url)
      .then((resp) => resp.json())
      .then((data) => { processResult(data); })
      .catch(function(error) {
        console.log(error);
      });

  }

  function getPrevious(){
    counter = counter - 1;
    const url = './includes/functions.php?id=' + counter;

    fetch(url)
      .then((resp) => resp.json())
      .then((data) => { processResult(data); })
      .catch(function(error) {
        console.log(error);
      });

  }

  function processResultLoad(data) {
        const { pitems_image, pitems_title, pitems_desc, pitems_link_src, pitems_link_text} = data;
        let image = document.querySelector('#lightboxImage').src = "img/" + pitems_image;
        let title = document.querySelector('#title').innerHTML = pitems_title;
        let desc = document.querySelector('#desc').innerHTML = pitems_desc;
        let thumbImage = document.querySelector('#workitem').style.backgroundImage = "url(../img/"+pitems_image+")";
        let thumbtitle = document.querySelector('#workTitle').innerHTML = pitems_title;
        let link = document.querySelector('#lightboxLink').innerHTML = pitems_link_text;
        link.href = pitems_link_src;

 }

  function processResult(data) {
      if (data == null){
        if (counter > 1){
          counter = counter - 1;
        } else if(counter < 1){
          counter = counter + 1;
        }
      } else{
        const { pitems_image, pitems_title, pitems_desc, pitems_link_src, pitems_link_text} = data;
        let image = document.querySelector('#lightboxImage').src = "img/" + pitems_image;
        let title = document.querySelector('#title').innerHTML = pitems_title;
        let desc = document.querySelector('#desc').innerHTML = pitems_desc;
        let thumbImage = document.querySelector('#workitem').style.backgroundImage = "url(../img/"+pitems_image+")";
        let thumbtitle = document.querySelector('#workTitle').innerHTML = pitems_title;
        let link = document.querySelector('#lightboxLink').innerHTML = pitems_link_text;
        let linksrc = document.querySelector('#lightboxLink').href = pitems_link_src;
    }
 }



 window.addEventListener('load', loadup, false);
 window.addEventListener('load', loadSkills, false);
 right.addEventListener('click', getNext, false);
 left.addEventListener('click', getPrevious, false);



})();
