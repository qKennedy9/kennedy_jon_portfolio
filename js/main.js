(function(){

  var opener = document.querySelector('#opener');
  var hamburgers = document.querySelectorAll('.hamburger');
  var mobileNav = document.querySelector('#mainNav');
  var closer = document.querySelector('#closer');
  var links = document.querySelectorAll('.link');
  var sections = document.querySelectorAll('.siteSection');
  var logo = document.querySelector('#logo');
  var scrollTop = window.pageYOffset;
  var width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
  var counter = 0;
  var time;
  var logos = document.querySelectorAll('.logos');
  var lightbox = document.querySelector('#lightboxContainer');
  var lightboxTrigger = document.querySelector('#workitem');
  var lightboxCloser = document.querySelector('#lightboxClose');
  var colorClassesAbout = ["aboutToWork","aboutToSkills","aboutToContact"];
  var colorClassesWork = ["workToSkills","workToContact","workToAbout"];
  var colorClassesSkills = ["skillsToContact","skillsToWork","skillsToAbout"];
  var colorClassesContact = ["contactToSkills","contactToWork","contactToAbout"];
  var overlayColours = ["rgba(77,244,144,0.7)","rgba(141,78,242,0.7)","rgba(239,80,80,0.7)","rgba(80,209,239,0.7)"];
  var colors = ["#4df490","#8d4ef2","#ef5050","#50d1ef"];
  var linkTransitionsDown = ["aboutToWork","workToSkills","skillsToContact"];
  var linkTransitionsUp = ["workToAbout", "skillsToWork", "contactToSkills"];




  function openMenu(){
    slide();
    linkFade();
  }

  function slide(){
    mobileNav.style.width = '100%';
    for (var i = 0; i < hamburgers.length; i ++){
      hamburgers[i].style.display = 'none';
    }
    for (var b = 0; b < logos.length; b ++){
      logos[b].style.display = 'none';
    }
  }

  function closeMenu(){
    mobileNav.style.width = '0%';
    for (var i = 0; i < hamburgers.length; i ++){
      hamburgers[i].style.display = 'block';
    }
    for (var i = 0; i < links.length; i ++){
      links[i].classList.remove('linkFade');
    }
    for (var b = 0; b < logos.length; b ++){
      logos[b].style.display = 'block';
    }
  }

  function linkFade(){
    for (var i = 0; i < links.length; i ++){
      if (i == 0){
        links[i].classList += ' linkFade';
      }else if (i == 1){
        links[i].classList += ' linkFade';
        links[i].style.animationDelay = '0.5s';
      } else if (i == 2){
        links[i].classList += ' linkFade';
        links[i].style.animationDelay = '1s';
      } else if (i == 3){
        links[i].classList += ' linkFade';
        links[i].style.animationDelay = '1.5s';
      } else{

      }
    }
  }

  function navigation(){
      clearTimeout(time);
      time = setTimeout(navigationSort, 300);
  }

  function navigationSort(){
    var scrollTop2 = window.pageYOffset;
    var scrollTop3 = scrollTop2 - scrollTop;
    if (scrollTop2 > scrollTop && scrollTop3 >= 150){
      if (counter < 3){
      counter = counter + 1;
      scrollTop = scrollTop2;
      scrolldown();
      }
    } else if(scrollTop2 < scrollTop  && scrollTop3 <= -150){
      if (counter > 0){
        counter = counter -1;
        scrollTop = scrollTop2;
        scrollUp();
      }
    }
  }

  function scrolldown(){
    sections[counter-1].classList.remove('sectionIn');
    sections[counter-1].classList += " sectionOut";
    setTimeout(function(){
      sections[counter-1].style.display = 'none';
      sections[counter].style.display = 'block';
    }, 800);
    sections[counter].classList += " sectionIn";
    if (width < 1279){
      mobileNav.style.background = overlayColours[counter];
    }
    for (var c = 0; c < logos.length; c ++){
      if (c >= counter){
        logos[c].classList.remove("fadeout");
        hamburgers[c].classList.remove("fadeout");
      }
    }
    logos[counter-1].classList += " fadeout";
    logos[counter-1].classList.remove("linkFade");
    hamburgers[counter-1].classList += " fadeout";
    hamburgers[counter-1].classList.remove("linkFade");
    for (var i = 0; i < links.length; i++){
      for (var b = 0; b < colorClassesWork.length; b ++){
        links[i].classList.remove(colorClassesAbout[b]);
        links[i].classList.remove(colorClassesWork[b]);
        links[i].classList.remove(colorClassesSkills[b]);
        links[i].classList.remove(colorClassesContact[b]);
      }
      links[i].classList += " "+linkTransitionsDown[counter-1];
      links[i].classList.remove(linkTransitionsDown[counter-2]);
      links[i].classList.remove(linkTransitionsUp[counter-1]);
      links[i].style.borderColor = colors[counter];
    }



  }

  function scrollUp(){

    sections[counter+1].classList.remove('sectionIn');
    sections[counter+1].classList += " sectionOut";
    setTimeout(function(){
      sections[counter+1].style.display = 'none';
      sections[counter].style.display = 'block';
    }, 800);
    sections[counter].classList += " sectionIn";
    if (width < 1279){
      mobileNav.style.background = overlayColours[counter];
    }
    logos[counter].classList += " linkFade";
    logos[counter].classList.remove("fadeout");
    hamburgers[counter].classList += " linkFade";
    hamburgers[counter].classList.remove("fadeout");
    for (var i = 0; i < links.length; i++){
      for (var b = 0; b < colorClassesWork.length; b ++){
        links[i].classList.remove(colorClassesAbout[b]);
        links[i].classList.remove(colorClassesWork[b]);
        links[i].classList.remove(colorClassesSkills[b]);
        links[i].classList.remove(colorClassesContact[b]);
      }
      links[i].classList += " "+linkTransitionsUp[counter];
      links[i].classList.remove(linkTransitionsUp[counter+1]);
      links[i].classList.remove(linkTransitionsDown[counter]);
      links[i].style.borderColor = colors[counter];
    }

  }

  function openLightbox(){
    lightbox.style.display = "block";
  }

  function closeLightbox(){
    lightbox.style.display = "none";
  }

  function navTransition(){
    if (counter == 0){
      if (this.id == "workLink"){
        counter = 1;
        if (width < 1279){
          mobileNav.style.background = overlayColours[counter];
        }
        sections[0].classList.remove("sectionIn");
        sections[0].classList += " sectionOut";
        setTimeout(function(){
          sections[0].style.display = 'none';
          sections[1].style.display = 'block';
        }, 800);
        sections[1].classList.remove("sectionOut");
        sections[1].classList += " sectionIn";
        for (var i = 0; i < links.length; i ++){
          for (var b = 0; b < colorClassesAbout.length; b ++){
            links[i].classList.remove(colorClassesAbout[b]);
            links[i].classList.remove(colorClassesWork[b]);
            links[i].classList.remove(colorClassesSkills[b]);
            links[i].classList.remove(colorClassesContact[b]);
            }
          links[i].classList += " "+colorClassesAbout[0];
          links[i].style.borderColor = colors[counter];
          }
          for (var b = 0; b < logos.length; b ++){
            logos[b].classList.remove('linkFade');
            logos[b].classList += " fadeout";
            hamburgers[b].classList.remove('linkFade');
            hamburgers[b].classList += " fadeout";
          }
          logos[counter].classList += " linkFade";
          hamburgers[counter].classList += " linkFade";


        } else if (this.id == "skillLink"){
          counter = 2;
          if (width < 1279){
            mobileNav.style.background = overlayColours[counter];
          }
          sections[0].classList.remove("sectionIn");
          sections[0].classList += " sectionOut";
          setTimeout(function(){
            sections[0].style.display = 'none';
            sections[2].style.display = 'block';
          }, 800);
          sections[2].classList.remove("sectionOut");
          sections[2].classList += " sectionIn";
        for (var i = 0; i < links.length; i ++){
          for (var b = 0; b < colorClassesAbout.length; b ++){
            links[i].classList.remove(colorClassesAbout[b]);
            links[i].classList.remove(colorClassesWork[b]);
            links[i].classList.remove(colorClassesSkills[b]);
            links[i].classList.remove(colorClassesContact[b]);
            }
            links[i].classList += " "+colorClassesAbout[1];
            links[i].style.borderColor = colors[counter];
          }
          for (var b = 0; b < logos.length; b ++){
            logos[b].classList.remove('linkFade');
            logos[b].classList += " fadeout";
            hamburgers[b].classList.remove('linkFade');
            hamburgers[b].classList += " fadeout";
          }
          logos[counter].classList += " linkFade";
          hamburgers[counter].classList += " linkFade";
    } else if (this.id == "contactLink"){
      counter = 3;
      if (width < 1279){
        mobileNav.style.background = overlayColours[counter];
      }
      sections[0].classList.remove("sectionIn");
      sections[0].classList += " sectionOut";
      setTimeout(function(){
        sections[0].style.display = 'none';
        sections[3].style.display = 'block';
      }, 800);
      sections[3].classList.remove("sectionOut");
      sections[3].classList += " sectionIn";
      for (var i = 0; i < links.length; i ++){
        for (var b = 0; b < colorClassesAbout.length; b ++){
          links[i].classList.remove(colorClassesAbout[b]);
          links[i].classList.remove(colorClassesWork[b]);
          links[i].classList.remove(colorClassesSkills[b]);
          links[i].classList.remove(colorClassesContact[b]);
        }
        links[i].classList += " "+colorClassesAbout[2];
        links[i].style.borderColor = colors[counter];
    }
    for (var b = 0; b < logos.length; b ++){
      logos[b].classList.remove('linkFade');
      logos[b].classList += " fadeout";
      hamburgers[b].classList.remove('linkFade');
      hamburgers[b].classList += " fadeout";
    }
    logos[counter].classList += " linkFade";
    hamburgers[counter].classList += " linkFade";
  }
  }else if (counter == 1){
      if (this.id == "aboutLink"){
        counter = 0;
        if (width < 1279){
          mobileNav.style.background = overlayColours[counter];
        }
        sections[1].classList.remove("sectionIn");
        sections[1].classList += " sectionOut";
        setTimeout(function(){
          sections[1].style.display = 'none';
          sections[0].style.display = 'block';
        }, 800);
        sections[0].classList.remove("sectionOut");
        sections[0].classList += " sectionIn";
        for (var i = 0; i < links.length; i ++){
          for (var b = 0; b < colorClassesAbout.length; b ++){
            links[i].classList.remove(colorClassesAbout[b]);
            links[i].classList.remove(colorClassesWork[b]);
            links[i].classList.remove(colorClassesSkills[b]);
            links[i].classList.remove(colorClassesContact[b]);
            }
          links[i].classList += " "+colorClassesWork[2];
          links[i].style.borderColor = colors[counter];
          }
          for (var b = 0; b < logos.length; b ++){
            logos[b].classList.remove('linkFade');
            logos[b].classList += " fadeout";
            hamburgers[b].classList.remove('linkFade');
            hamburgers[b].classList += " fadeout";
          }
          logos[counter].classList += " linkFade";
          hamburgers[counter].classList += " linkFade";
        } else if (this.id == "skillLink"){
          counter = 2;
          if (width < 1279){
            mobileNav.style.background = overlayColours[counter];
          }
          sections[1].classList.remove("sectionIn");
          sections[1].classList += " sectionOut";
          setTimeout(function(){
            sections[1].style.display = 'none';
            sections[2].style.display = 'block';
          }, 800);
          sections[2].classList.remove("sectionOut");
          sections[2].classList += " sectionIn";
        for (var i = 0; i < links.length; i ++){
          for (var b = 0; b < colorClassesAbout.length; b ++){
            links[i].classList.remove(colorClassesAbout[b]);
            links[i].classList.remove(colorClassesWork[b]);
            links[i].classList.remove(colorClassesSkills[b]);
            links[i].classList.remove(colorClassesContact[b]);
            }
            links[i].classList += " "+colorClassesWork[0];
            links[i].style.borderColor = colors[counter];
          }
          for (var b = 0; b < logos.length; b ++){
            logos[b].classList.remove('linkFade');
            logos[b].classList += " fadeout";
            hamburgers[b].classList.remove('linkFade');
            hamburgers[b].classList += " fadeout";
          }
          logos[counter].classList += " linkFade";
          hamburgers[counter].classList += " linkFade";
    } else if (this.id == "contactLink"){
      counter = 3;
      if (width < 1279){
        mobileNav.style.background = overlayColours[counter];
      }
      sections[1].classList.remove("sectionIn");
      sections[1].classList += " sectionOut";
      setTimeout(function(){
        sections[1].style.display = 'none';
        sections[3].style.display = 'block';
      }, 800);
      sections[3].classList.remove("sectionOut");
      sections[3].classList += " sectionIn";
      for (var i = 0; i < links.length; i ++){
        for (var b = 0; b < colorClassesAbout.length; b ++){
          links[i].classList.remove(colorClassesAbout[b]);
          links[i].classList.remove(colorClassesWork[b]);
          links[i].classList.remove(colorClassesSkills[b]);
          links[i].classList.remove(colorClassesContact[b]);
        }
        links[i].classList += " "+colorClassesWork[1];
        links[i].style.borderColor = colors[counter];
    }
    for (var b = 0; b < logos.length; b ++){
      logos[b].classList.remove('linkFade');
      logos[b].classList += " fadeout";
      hamburgers[b].classList.remove('linkFade');
      hamburgers[b].classList += " fadeout";
    }
    logos[counter].classList += " linkFade";
    hamburgers[counter].classList += " linkFade";
  }


} else if (counter == 2){
  if (this.id == "aboutLink"){
    counter = 0;
    if (width < 1279){
      mobileNav.style.background = overlayColours[counter];
    }
    sections[2].classList.remove("sectionIn");
    sections[2].classList += " sectionOut";
    setTimeout(function(){
      sections[2].style.display = 'none';
      sections[0].style.display = 'block';
    }, 800);
    sections[0].classList.remove("sectionOut");
    sections[0].classList += " sectionIn";
    for (var i = 0; i < links.length; i ++){
      for (var b = 0; b < colorClassesAbout.length; b ++){
        links[i].classList.remove(colorClassesAbout[b]);
        links[i].classList.remove(colorClassesWork[b]);
        links[i].classList.remove(colorClassesSkills[b]);
        links[i].classList.remove(colorClassesContact[b]);
        }
      links[i].classList += " "+colorClassesSkills[2];
      links[i].style.borderColor = colors[counter];
      }
      for (var b = 0; b < logos.length; b ++){
        logos[b].classList.remove('linkFade');
        logos[b].classList += " fadeout";
        hamburgers[b].classList.remove('linkFade');
        hamburgers[b].classList += " fadeout";
      }
      logos[counter].classList += " linkFade";
      hamburgers[counter].classList += " linkFade";
    } else if (this.id == "workLink"){
      counter = 1;
      sections[2].classList.remove("sectionIn");
      sections[2].classList += " sectionOut";
      setTimeout(function(){
        sections[2].style.display = 'none';
        sections[1].style.display = 'block';
      }, 800);
      sections[1].classList.remove("sectionOut");
      sections[1].classList += " sectionIn";
    for (var i = 0; i < links.length; i ++){
      for (var b = 0; b < colorClassesAbout.length; b ++){
        links[i].classList.remove(colorClassesAbout[b]);
        links[i].classList.remove(colorClassesWork[b]);
        links[i].classList.remove(colorClassesSkills[b]);
        links[i].classList.remove(colorClassesContact[b]);
        }
        links[i].classList += " "+colorClassesSkills[1];
        links[i].style.borderColor = colors[counter];
      }
      for (var b = 0; b < logos.length; b ++){
        logos[b].classList.remove('linkFade');
        logos[b].classList += " fadeout";
        hamburgers[b].classList.remove('linkFade');
        hamburgers[b].classList += " fadeout";
      }
      logos[counter].classList += " linkFade";
      hamburgers[counter].classList += " linkFade";
} else if (this.id == "contactLink"){
  counter = 3;
  if (width < 1279){
    mobileNav.style.background = overlayColours[counter];
  }
  sections[2].classList.remove("sectionIn");
  sections[2].classList += " sectionOut";
  setTimeout(function(){
    sections[2].style.display = 'none';
    sections[3].style.display = 'block';
  }, 800);
  sections[3].classList.remove("sectionOut");
  sections[3].classList += " sectionIn";
  for (var i = 0; i < links.length; i ++){
    for (var b = 0; b < colorClassesAbout.length; b ++){
      links[i].classList.remove(colorClassesAbout[b]);
      links[i].classList.remove(colorClassesWork[b]);
      links[i].classList.remove(colorClassesSkills[b]);
      links[i].classList.remove(colorClassesContact[b]);
    }
    links[i].classList += " "+colorClassesSkills[0];
    links[i].style.borderColor = colors[counter];
}
for (var b = 0; b < logos.length; b ++){
  logos[b].classList.remove('linkFade');
  logos[b].classList += " fadeout";
  hamburgers[b].classList.remove('linkFade');
  hamburgers[b].classList += " fadeout";
}
logos[counter].classList += " linkFade";
hamburgers[counter].classList += " linkFade";
}
}else if (counter == 3){
  if (this.id == "aboutLink"){
    counter = 0;
    if (width < 1279){
      mobileNav.style.background = overlayColours[counter];
    }
    sections[3].classList.remove("sectionIn");
    sections[3].classList += " sectionOut";
    setTimeout(function(){
      sections[3].style.display = 'none';
      sections[0].style.display = 'block';
    }, 800);
    sections[0].classList.remove("sectionOut");
    sections[0].classList += " sectionIn";
    for (var i = 0; i < links.length; i ++){
      for (var b = 0; b < colorClassesAbout.length; b ++){
        links[i].classList.remove(colorClassesAbout[b]);
        links[i].classList.remove(colorClassesWork[b]);
        links[i].classList.remove(colorClassesSkills[b]);
        links[i].classList.remove(colorClassesContact[b]);
        }
      links[i].classList += " "+colorClassesContact[2];
      links[i].style.borderColor = colors[counter];
      }
      for (var b = 0; b < logos.length; b ++){
        logos[b].classList.remove('linkFade');
        logos[b].classList += " fadeout";
        hamburgers[b].classList.remove('linkFade');
        hamburgers[b].classList += " fadeout";
      }
      logos[counter].classList += " linkFade";
      hamburgers[counter].classList += " linkFade";
    } else if (this.id == "workLink"){
      counter = 1;
      sections[3].classList.remove("sectionIn");
      sections[3].classList += " sectionOut";
      setTimeout(function(){
        sections[3].style.display = 'none';
        sections[1].style.display = 'block';
      }, 800);
      sections[1].classList.remove("sectionOut");
      sections[1].classList += " sectionIn";
    for (var i = 0; i < links.length; i ++){
      for (var b = 0; b < colorClassesAbout.length; b ++){
        links[i].classList.remove(colorClassesAbout[b]);
        links[i].classList.remove(colorClassesWork[b]);
        links[i].classList.remove(colorClassesSkills[b]);
        links[i].classList.remove(colorClassesContact[b]);
        }
        links[i].classList += " "+colorClassesContact[1];
        links[i].style.borderColor = colors[counter];
      }
      for (var b = 0; b < logos.length; b ++){
        logos[b].classList.remove('linkFade');
        logos[b].classList += " fadeout";
        hamburgers[b].classList.remove('linkFade');
        hamburgers[b].classList += " fadeout";
      }
      logos[counter].classList += " linkFade";
      hamburgers[counter].classList += " linkFade";
} else if (this.id == "skillLink"){
  counter = 3;
  if (width < 1279){
    mobileNav.style.background = overlayColours[counter];
  }
  sections[3].classList.remove("sectionIn");
  sections[3].classList += " sectionOut";
  setTimeout(function(){
    sections[3].style.display = 'none';
    sections[2].style.display = 'block';
  }, 800);
  sections[2].classList.remove("sectionOut");
  sections[2].classList += " sectionIn";
  for (var i = 0; i < links.length; i ++){
    for (var b = 0; b < colorClassesAbout.length; b ++){
      links[i].classList.remove(colorClassesAbout[b]);
      links[i].classList.remove(colorClassesWork[b]);
      links[i].classList.remove(colorClassesSkills[b]);
      links[i].classList.remove(colorClassesContact[b]);
    }
    links[i].classList += " "+colorClassesWork[0];
    links[i].style.borderColor = colors[counter-1];
}
for (var b = 0; b < logos.length; b ++){
  logos[b].classList.remove('linkFade');
  logos[b].classList += " fadeout";
  hamburgers[b].classList.remove('linkFade');
  hamburgers[b].classList += " fadeout";
}
logos[counter-1].classList.remove("fadeout");
logos[counter-1].classList += " linkFade";
hamburgers[counter-1].classList.remove("fadeout");
hamburgers[counter-1].classList += " linkFade";
}
}
}


  opener.addEventListener('click',openMenu,false);
  closer.addEventListener('click',closeMenu,false);
  window.addEventListener('scroll',navigation,false);
  lightboxTrigger.addEventListener('click', openLightbox,false);
  lightboxClose.addEventListener('click', closeLightbox, false);
  for(var i = 0; i < links.length; i++){
    links[i].addEventListener('click', navTransition, false);
    if (width < 1279){
      links[i].addEventListener('click', closeMenu, false);
    }

  }

})();
