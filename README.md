# Kennedy_Jon_Portfolio

Portfolio Website for Quinn Kennedy Design and Development. All files necessary for the site to function are in this repo, with the exception of node_modules for grunt in order to save space.

Technologies Uses:
Grunt
Sass
Javscript
HTML 5 
CSS
PHP
AJAX

How To Run:
Place the site folder into the htdocs folder of wamp or mamp, and the site should one as normal from there.

